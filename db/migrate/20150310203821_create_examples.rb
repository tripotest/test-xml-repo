class CreateExamples < ActiveRecord::Migration
  def change
    create_table :examples do |t|
      t.string    :name
      t.string    :param_1
      t.timestamp :param_2
      t.string    :some_code
      t.float     :some_price_param
      # t.timestamps null: false
    end
  end
end
