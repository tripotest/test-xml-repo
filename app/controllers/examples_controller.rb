class ExamplesController < ApplicationController
  before_action :set_example, only: [:show, :update, :destroy]

  # GET /examples
  # GET /examples.json
  def index
    @examples = Example.all

    render xml: @examples
  end

  # GET /examples/1
  # GET /examples/1.json
  def show
    render json: @example
  end

  # POST /examples
  # POST /examples.json
  def create
    @example = Example.new(example_params)

    if @example.save
      render json: @example, status: :created, location: @example
    else
      render json: @example.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /examples/1
  # PATCH/PUT /examples/1.json
  def update
    @example = Example.find(params[:id])

    if @example.update(example_params)
      head :no_content
    else
      render json: @example.errors, status: :unprocessable_entity
    end
  end

  # DELETE /examples/1
  # DELETE /examples/1.json
  def destroy
    @example.destroy

    head :no_content
  end

  private

    def set_example
      @example = Example.find(params[:id])
    end

    def example_params
      params[:example]
    end
end
